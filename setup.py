import setuptools

# the magic pip upload command is
# python setup.py sdist upload -r local


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="AIC_test", # Replace with your own username
    version="1.1.2",
    author="Rob Thomas",
    author_email="rob.thomas@lhasalimited.org",
    description="An AIC test for NOAEL/LOAEL calculation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.0',
)
