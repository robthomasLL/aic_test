# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:44:46 2021

@author: RobThomas
"""


import numpy as np
from scipy.stats import norm

import sklearn.isotonic

import collections
AIC_results = collections.namedtuple('AIC_results',['increased','DoF','score','rates', 'totalOdds', 'odds','is_trend'])





def fitRates(observations,stds,DoF):
	'''This is going to be very tricky to do properly. I want to find rates that maximise the score except they must be monotonic and have only N DoF.
	This is only an approximation, but I think it's a good one.'''

	O = np.log(observations)
	S = np.asarray(stds/observations)
	nPnts = len(O)
	# weight = 1/var
	w = 1/(S**2)

	# get a monotonic fit to the probabilities
	X = np.arange(nPnts)
	mono = list(sklearn.isotonic.IsotonicRegression().fit_transform(X,O,sample_weight=w))

	# now take the N greatest changes in the monotonic fit, this is the amount it changes AFTER position n. these must be > 0
	dMono = np.diff(mono+mono[-1])

	inds = [(x,d) for x,d in zip(X,dMono) if not d==0] # stick in tuples filtering out 0's
	inds.sort(key=lambda x: abs(x[1])) # sort
	inds = [i[0]+1 for i in inds[:DoF]] + [nPnts] # unpack top N from tuples
	inds.sort()

	# now construct rates based on the MLE of the sections between increases
	rates = []
	start = 0
	for stop in inds:
		N = stop-start
		rate = np.average(O[start:stop],weights=w[start:stop])
		rates +=[rate]*N
		start = stop

	return np.exp(rates)



def modelScore(observations,stds,predictions,DoF):
	# returns a penalised log-likelihood of a model given findings, rates, and the DoF used
	# likelihoods are based off log-normal distributions
	# returned score is the AIC

	# check everything is the right length
	assert len(observations) == len(stds) == len(predictions), 'Length mismatch.'

	O = np.log(observations)
	S = stds/observations
	P = np.log(predictions)

	# start the score with the DoF penalty
	score = DoF

	for o,s,p in zip(O,S,P):
		score -= norm.logpdf(o,loc=p,scale=s)

	return 2*score




def AIC_Test(observations,stds=None,returnAll=False):
	'''My implementation of an AIC test for bernoulli trials. Takes an ordered list of observations and sample sizes.
	By default returns a list of o/1 for no change/increased, but it can also return further details.'''

	# if the observations is a findings dict we can get the ordered lists from it
	if isinstance(observations,dict):
		observations,stds = convertFindingsDict(observations)
	elif stds is None:
		raise ValueError('No sample size given.')

	assert len(observations) == len(stds), f'{len(observations)} counts, {len(stds)} sample sizes.'
	assert len(observations) > 1, f'{len(observations)} items found, must be greater than 1.'

	predictions = []

	# get predicted rates and scores for all my DoFs
	for DoF in range(len(observations)):
		rates = fitRates(observations,stds,DoF)
		score = modelScore(observations,stds,rates,DoF)
		predictions.append((score,rates,DoF))
		assert np.isfinite(score), f'Error fitting {observations}, {stds}, DoF = {DoF}'

	# get best prediction
	predictions.sort(key=lambda x:x[0])
	score,rates,DoF = predictions[0]

	# return an array of increased and normal indices
	base = rates[0]
	increased = np.array([r>base for r in rates])

	if returnAll:

		# calculate odds based on best fit with an extra DoF
		increases = [i for i in predictions if i[2]>0]
		_,increasedRates,_ = increases[0]
		trend = any(increased)

		totalOdds,odds = _calculateOdds(predictions)
		return AIC_results(increased,DoF,score,rates, totalOdds, odds, trend)
	else:
		return increased



def _calculateOdds(models):
	'''calculate odds for the observed rates compared to a flat response
	 I think this is mathematically valid but haven't done the full working so don't rely on it'.'''


	# get a dict of DoF -> AIC
	dofs = {model[0]:model[2] for model in models}

	# get propabilities of the different DoF
	ps = _AICWeights(dofs)

	# return the p values for the minimum and the individual values
	k = min(ps.keys())
	return ps[k],ps



def _AICWeights(aics):
	# calculate AIC weights, optionally giving no weights to models
	# that are worst than the best by some threshold

	if isinstance(aics,dict):
		keys = aics.keys()
		weights = _AICWeights(list(aics.values()))
		return {k:w for k,w in zip(keys,weights)}

	aics = np.array(aics)
	delta = aics-aics.min()
	w = np.exp(-0.5*delta)

	return  w/w.sum()



def convertFindingsDict(findings):
	'''converts a dose keyed findings dictionary to occance,stds lists.'''

	doses = sorted(findings.keys())

	obs = [findings[dose] for dose in doses]
	observations,stdss = zip(*obs)

	return np.array(observations),np.array(stdss)



if __name__ == '__main__':


	x = [0,1,2,3,4]
	y = [0.01,1,3,2,4]
	n = [i*0.5 for i in y]

	data = {X:(Y,N) for X,Y,N in zip(x,y,n)}
	data


	out = AIC_Test(data,returnAll=True)
	print(out)



