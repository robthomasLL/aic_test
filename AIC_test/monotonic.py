# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 08:47:15 2022

@author: RobThomas
"""


import numpy as np
import scipy.optimize
import scipy.interpolate
import scipy.stats

import collections
import itertools
import sklearn.isotonic


def _buildConstraintsMatrix(n):

	rows = []

	for i in range(n):
		for j in range(i):
			#print(i,j)
			r = np.zeros(n)
			r[i] = 1
			r[j] = -1
			rows.append(r)

	return np.vstack(rows)


def _makeLogLLF(obs,ss):

	def F(x):
		LL = sum(scipy.stats.binom.logpmf(obs,ss,x))
		return -LL

	return F

def _makeX0(x,obs,ss):
	n = len(obs)
	x0 = sum(obs) / sum(ss)
	x0 = x0*np.ones(n)
	return x0

def fitMonotonicBinomial(obs,ss,x=None,bounds=(0,1),ascending=True):

	# make sure sample size is per observation
	if isinstance(ss,int):
		ss = [ss]*len(obs)
	ss = np.asarray(ss)

	# if x isn't given use 0 to N
	if x is None:
		x = np.arange(len(obs))

	n = len(obs)
	# generate constraint matrix
	constraints = _buildConstraintsMatrix(n)

	# make bounds and constraints
	if ascending:
		C = scipy.optimize.LinearConstraint(constraints,0,np.inf)
	else:
		C = scipy.optimize.LinearConstraint(constraints,-np.inf,0)

	B = scipy.optimize.Bounds(*bounds)

	# get initial guess
	x0 = _makeX0(x,obs,ss)
	check = constraints.dot(x0)
	assert np.all(check>=0), f'Initial guess does not fit constraints. {check}'

	# make LL function
	F = _makeLogLLF(obs, ss)

	# solve
	res = scipy.optimize.minimize(F,x0,constraints=C,bounds=B)

	# build interpolator
	interp = scipy.interpolate.interp1d(x,res.x)

	MonotonicFit = collections.namedtuple('MonotonicFit', ['values','fit','F'])
	return MonotonicFit(res.x,res, interp)




class MonotonicProblem:

	def __init__(self,obs,ss,inds=None):

		self.obs = obs
		self.ss = ss
		self.inds = list(inds)
		self.N = len(obs)

	def buildRates(self,x):
		base, deltas = x[0],x[1:]
		rates = base*np.ones(self.N).astype(float)

		for i,d in zip(self.inds,deltas):
			rates[i:] += (1-rates[i])*d

		return rates

	def LL(self,x):
		rates = self.buildRates(x)
		return -scipy.stats.binom.logpmf(self.obs,self.ss,rates).sum()

	def solve(self):
		x0 = [1/sum(self.ss)] + list(np.zeros_like(self.inds))
		bounds = [(0,1)] + [(0,1) for i in self.inds]

		r = scipy.optimize.minimize(self.LL,x0,bounds=bounds,method='TNC')
		return self.buildRates(r.x),r



def bruteForceRates(occurances,sampleSize,DoF):

	inds = np.arange(1,len(occurances))
	best_fit = np.inf

	for test_ind in itertools.combinations(inds,DoF):
		P = MonotonicProblem(occurances,sampleSize,test_ind)
		score,rates = P.solve()

		# update the best fit
		if score.fun < best_fit:
			best_rates = rates.copy()
			best_fit = score.fun
			#print(f'score = {score}, rates = {rates}')

	return best_rates





def fitRates(occurances,sampleSize,DoF,method='isotonic'):
	'''This is going to be very tricky to do properly. I want to find rates that maximise the score except they must be monotonic and have only N DoF.
	This is only an approximation, but I think it's a good one.'''

	# get a monotonic fit to the probabilities
	probs = [o/s for o,s in zip(occurances,sampleSize)]
	X = list(range(len(probs)))

	if method == 'isotonic':
		mono = list(sklearn.isotonic.IsotonicRegression(increasing=True).fit_transform(X,probs))
	elif method == 'convex':
		mono = fitMonotonicBinomial(occurances,sampleSize).values
	else:
		raise ValueError(f'Bad fitting method {method}. Should be "isotonic" or "convex".')

	# now take the N greatest changes in the monotonic fit, this is the amount it changes AFTER position n. these must be > 0
	dMono = np.diff(mono+mono[-1])
	inds = [(x,d) for x,d in zip(X,dMono) if d>0] # stick in tuples filtering out 0's
	inds.sort(key=lambda x: x[1]) # sort
	inds = [i[0]+1 for i in inds[:DoF]] + [len(probs)] # unpack top N from tuples
	inds.sort()

	# now construct rates based on the MLE of the sections between increases
	rates = []
	start = 0
	for stop in inds:
		N = stop-start
		O = sum(occurances[start:stop])
		S = sum(sampleSize[start:stop])
		rates +=[O/S]*N
		start = stop

	return rates















