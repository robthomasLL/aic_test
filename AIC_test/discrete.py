# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 13:09:04 2021

@author: RobThomas
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 11:10:32 2019

An AIC test implementation


@author: RobThomas rob.thomas@lhasalimited.org
"""

import numpy as np
from scipy.stats import binom # binom.pdf(nOccurances,sampleSize,P)

import statsmodels.stats.proportion
import matplotlib.pyplot as plt

import collections
AIC_results = collections.namedtuple('AIC_results',['increased','DoF','score','rates', 'totalOdds', 'odds','is_trend'])

try:
	from . import monotonic
except:
	import monotonic


def modelScore(occurances,sampleSize,Ps,DoF):
	# returns a penalised log-likelihood of a model given findings, rates, and the DoF used
	# likelihoods are based off binomial distributions
	# returned score is the AIC

	# check everything is the right length
	assert len(occurances) == len(sampleSize) == len(Ps), 'Length mismatch.'

	# start the score with the DoF penalty
	score = DoF # DoF = k in standard notation

	for o,s,p in zip(occurances,sampleSize,Ps):
		score -= np.log(binom.pmf(o,s,p))

	AIC = 2*score
	return AIC


def fitRates(occurances,sampleSize,DoF,method='isotonic'):
	'''This is going to be very tricky to do properly. I want to find rates that maximise the score except they must be monotonic and have only N DoF.
	This is only an approximation, but I think it's a good one.'''

	if method == 'brute':
		return monotonic.bruteForceRates(occurances,sampleSize,DoF)
	else:
		return monotonic.fitRates(occurances,sampleSize,DoF,method)
	return rates




def AIC_Test(occurances,sampleSize=None,returnAll=False,method='isotonic'):
	'''My implementation of an AIC test for bernoulli trials. Takes an ordered list of occurances and sample sizes.
	By default returns a list of o/1 for no change/increased, but it can also return further details.'''

	# if the occurances is a findings dict we can get the ordered lists from it
	if isinstance(occurances,dict):
		occurances,sampleSize = convertFindingsDict(occurances)
	elif sampleSize is None:
		raise ValueError('No sample size given.')

	assert len(occurances) == len(sampleSize), f'{len(occurances)} counts, {len(sampleSize)} sample sizes.'
	assert len(occurances) > 1, f'{len(occurances)} items found, must be greater than 1.'
	assert min(sampleSize)>0, 'Sample sizes must be greater than 0.'

	predictions = []

	# get predicted rates and scores for all my DoFs
	for DoF in range(len(occurances)):
		rates = fitRates(occurances,sampleSize,DoF,method)
		score = modelScore(occurances,sampleSize,rates,DoF)
		predictions.append((score,rates,DoF))

	# get best prediction
	predictions.sort(key=lambda x:x[0])
	score,rates,DoF = predictions[0]

	# return an array of increased and normal indices
	base = rates[0]
	increased = np.array([r>base for r in rates])

	if returnAll:

		# calculate odds based on best fit with an extra DoF
		increases = [i for i in predictions if i[2]>0]
		_,increasedRates,_ = increases[0]
		trend = any(increased)

		totalOdds,odds = _calculateOdds(occurances,sampleSize,increasedRates)
		return AIC_results(increased,DoF,score,rates, totalOdds, odds,trend)
	else:
		return increased



def _calculateOdds(observed,sampleSize,rates):
	'''calculate odds for the observed rates compared to a flat response
	 I think this is mathematically valid but haven't done the full working so don't rely on it'.'''

	# log-P for no response
	k, n = sum(observed),sum(sampleSize)
	nullRate = k/n
	nullPs = np.array([binom.logpmf(o,s,nullRate) for o,s in zip(observed,sampleSize)])
	totalNullP = sum(nullPs)

	# calculate log-P for found rates
	ratePs = np.array([binom.logpmf(o,s,r) for o,s,r in zip(observed,sampleSize,rates)])
	totalRateP = sum(ratePs)

	# adjust for extra degrees of freedom by converting both to AIC
	totalNullP = (2*totalNullP) - 2
	k = len(set(rates))
	totalRateP = (2*totalRateP) - (2*k)

	# convert to odds
	totalOdds = np.exp(totalRateP-totalNullP)
	odds = np.exp(ratePs-nullPs)

	# convert to P
	totalOdds /= 1+totalOdds
	odds /= 1+odds
	return totalOdds,odds


def convertFindingsDict(findings):
	'''converts a dose keyed findings dictionary to occance,sampleSize lists.'''

	doses = sorted(findings.keys())

	obs = [findings[dose] for dose in doses if findings[dose][1] > 0]
	occurances,sampleSizes = zip(*obs)

	return occurances,sampleSizes


def plotTest(X,obs,ss):

	obs,ss = np.array(obs), np.array(ss)
	test_results = AIC_Test(obs,ss,returnAll=True)

	# get Y, upper, and lower errors
	Y = obs/ss
	lower,upper = statsmodels.stats.proportion.proportion_confint(obs,ss,method='jeffreys')

	plt.plot(X,Y,'ro',label='observed')
	plt.errorbar(X,Y,(Y-lower,upper-Y),fmt='ro')
	plt.step(X,test_results.rates,where='mid',label='model')
	plt.ylim((0,1))
	plt.legend()
	plt.xlabel('dose')
	plt.ylabel('response (proportion)')




def OAELs(increased,values):
	values = list(values)

	# if there are no increases return max and NaN
	if not any(increased):
		return max(values),np.nan

	x = np.argwhere(increased).flatten()[0]
	LOAEL = values[x]
	NOAEL = values[x-1]
	return NOAEL,LOAEL



if __name__ == '__main__':
	import random
	import pandas as pd
	import sklearn.metrics
	import matplotlib.pyplot as plt

	seed = 1
	random.seed(seed)
	np.random.seed(seed)

	method = 'isotonic'
	#method = 'convex'

	occur = [1,0,1,4,5,3]
	#occur = [2,0,1,4,1,0]
	ss = [10]*len(occur)

	increase, DoF, score,rates, totalOdds, odds, _ = AIC_Test(occur,ss,returnAll=True,method=method)
	print(increase)
	print(DoF)
	print(score)
	print(rates)
	print(totalOdds)
	print(odds)


	####### perform some tests so check general accuracy #####################


	def generateSamples(N,startP,stopP,SS=10):
		ps = np.linspace(startP,stopP,N)
		obs = binom.rvs(SS,ps)
		ss = np.array([SS]*N)
		return obs,ss

	def generatePs():
		 a = random.uniform(0.1, 0.9)**0.5
		 b = 1 -random.uniform(0.1, 0.9)**0.5
		 if a < b:
			 return a,b
		 else:
			 return b,a

	nTests = 500
	nDoses = 3
	SS = 10
	results = []

	# run tests with and without relationship
	for i in range(nTests):

		# get random P, alternating positive and no relationship
		rel = bool(i%2)

		if rel:
			p1,p2 = generatePs()
		else:
			p1 = random.uniform(0.1, 0.9)
			p2 = p1

		# get random observations
		obs,ss = generateSamples(nDoses,p1,p2,SS)

		# get increased and odds
		increased,_,_,_,totalOdds,_,_ = AIC_Test(obs,ss,returnAll=(True),method=method)
		increased = any(increased)

		# save results
		results.append(dict(p1=p1,p2=p2,increased=rel,positive=increased,odds=totalOdds,addedRisk=p2-p1))

	results = pd.DataFrame(results)
	print(sklearn.metrics.classification_report(results.increased,results.positive))

	# plot some results
	plt.hist(results.addedRisk,20) # added risk

	plt.figure()
	bins = np.linspace(0,1,20)
	for r,grp in results.groupby('increased'):
		plt.hist(grp.odds,bins,alpha=0.5)
	plt.show()

	plt.plot(results.addedRisk,results.odds,'o')

	plt.figure()
	plotTest([0,10,20],[1,2,3],[10,10,10])





