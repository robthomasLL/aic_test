# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 11:10:32 2019

An AIC test implementation


@author: RobThomas rob.thomas@lhasalimited.org
"""

from . import discrete
from . import continous

def AIC_Test(*args,**kwargs):
	raise NotImplementedError('AIC test has been moved to discrete.AIC_Test',)



